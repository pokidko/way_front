const gulp = require('gulp'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglifyjs'),
    cssnano = require('gulp-cssnano'),
    rename = require('gulp-rename'),
    del = require('del'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    cache = require('gulp-cache'),
    autoprefixer = require('gulp-autoprefixer'),
    browserSync = require('browser-sync'),
    bable =require('gulp-babel');

gulp.task('es6', function() {
    return gulp.src('js/main.js')
        .pipe(bable({
            presets: ['es2015']
        }))
        .pipe(uglify())
        .pipe(rename({
            suffix:'.min'
        }))
        .pipe(gulp.dest('js'))
});

gulp.task('sass', function(){
    return gulp.src(['sass/**/*.sass','sass/**/libs.sass'])
        .pipe(sass().on('error',sass.logError))
        .pipe(autoprefixer(['last 15 versions','>1%','ie 8','ie 7'],{cascade: true}))
        .pipe(gulp.dest('css'))
        .pipe(browserSync.reload({stream:true}));
});

gulp.task('scripts', function(){
    return gulp.src([
        // 'node_modules/jquery/dist/jquery.min.js',
        'node_modules/jquery-modal/jquery.modal.min.js',
        'node_modules/bootstrap/dist/js/bootstrap.js',
        'node_modules/wowjs/dist/js/wow.js.min',
        'node_modules/clipboard-js/clipboard.js',
        'node_modules/intl-tel-input/build/js/intlTelInput.js',
        'node_modules/intl-tel-input/build/js/utils.js',

    ])
        .pipe(concat('libs.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('js'));
});

gulp.task('fonts',function(){
    return gulp.src([
        'node_modules/bootstrap/dist/fonts/**/*',
        'node_modules/font-awesome/fonts/**/*',
        'fonts/**/*',
        'libs/font-awesome/fonts/**/*',
    ])
        .pipe(gulp.dest('fonts'));
});

gulp.task('css-libs',['sass'], function(){
    return gulp.src('css/libs.css')
        .pipe(cssnano())
        .pipe(rename({
            suffix:'.min'
        }))
        .pipe(gulp.dest('css'));
});


// gulp.task('img', function(){
//     return gulp.src('img/**/*')
//         .pipe(cache(imagemin({
//             interlaced: true,
//             progressive: true,
//             svgoPlugins: [{removeViewBox: false}],
//             une: [pngquant()]
//         })))
//         .pipe(gulp.dest('dist/img'));
// });

gulp.task('browser-sync',function(){
    browserSync({
        server: true,
        notyfi: false
    });
});

gulp.task('reload', function(){
    browserSync.reload();
});

gulp.task('watch', ['browser-sync','scripts' ,"css-libs"], function(){
    gulp.watch('sass/**/*.sass',['sass', 'reload']);
    gulp.watch('*.html');
    // gulp.watch('*.html',['reload']);
    gulp.watch('js/**/*.js',['es6']);
    // gulp.watch('js/**/*.js',['es6','reload']);
});

// gulp.task('build', ['clean', 'img', 'css-libs', 'scripts','fonts'], function(){
//
//     var buildCss= gulp.src([
//         'css/style.css',
//         'css/libs.min.css',
//     ])
//         .pipe(gulp.dest('dist/css'));
//     var buildfonts = gulp.src('fonts/**/*')
//         .pipe(gulp.dest('dist/fonts'));
//     var buildJs = gulp.src('js/**/*')
//         .pipe(gulp.dest('dist/js'));
//     var builHtml = gulp.src('*.html')
//         .pipe(gulp.dest('dist/'));
//
// });