/*
 * Author Alex Belokon
 */
"use strict";
var arrayCategory = [
    'Развлечение',
    'Дом, семья',
    'Обучение',
    'Путешествие',
    'Здоровье',
    'Прочее'
];
var arrayTarget = [
    [
        'Прыжок с парашютом',
        'Полёт на воздушном шаре',
        'Полёт в аэротрубе',
        'День в спа-салоне (центре)',
        'Занятся дайвингом',
        'Искупаться с дельфинами',
        'Побывать на чемпионате мира по хоккею/футболу',
        'Побывать на олимпиаде',
        'Записать песню, снять клип',
        'Исследовать океан на подводной лодке',
        'Съездить с семьей в Диснейленд'
    ],
    [
        'Построить коттедж',
        'Отпраздновать свадьбу, годовщину',
        'Кутить квартиру',
        'Улучшить жилищные условия',
        'Сделать капитальный ремонт',
        'Дача с баней'
    ],
    [
        'Закончить ВУЗ',
        'Получить второе высшее',
        'Получит лётные права',
        'Научиться игре на муз. инструменте'
    ],
    [
        'Тур по Европе',
        'Съездить во Францию',
        'На выходные в Венецию',
        'Кругосветное путешествие',
        'Тур на теплоходе',
        'Побывать в Антарктиде',
        'Уехать на зимовку в Таиланд'
    ],
    [
        'Оздоровить организм',
        'Пластическая операция',
        'Лечение ребёнка',
        'Вылечить зубы, протезирование',
        'ЭКО'
    ],
    [
        'Дорогой автомобиль',
        'Мотоцикл',
        'Профессиональный велосипед',
        'Лёгкий самолёт, вертолёт',
        'Яхта',
        'Погасить кредит'
    ]
];
var arrayCoef = [
    50,
    23,
    13,
    6,
    3,
    1.5,
    1,
    0.5,
    0.25
];

var view = {
    loginUserData: function (data) {
        $('.jsNavbar-right_login').hide();
        $('.jsNavbar-right_exit').show();
        $('.jsBtnLogin').hide();
        $('.jsBtnRegister').hide();
        var $infoUser = $('.jsInfoUser');
        var friendsTotal = data.w1+data.w2+data.w3+data.w4+data.w5+data.w6+data.w7+data.w8+data.w9;

        $infoUser.find('#user-total').text(data.balance + 'руб');
        $infoUser.find('#user-friends').text(friendsTotal);
        $infoUser.show();
    },
    buttonTimer: function ($selector, callback) {
        $selector.attr('disabled', true);
        var textBtn = $selector.text();
        var time = 60;

        var timerId = setInterval(function () {
            if (!time) {
                $selector.text(textBtn);
                $selector.removeAttr('disabled');
                callback();
                clearInterval(timerId);
                return;
            }
            $selector.text(time + ((time !== 1) ? ' cекунд' : ' сукунда'));
            time--;
        }, 1000);

    },
    showSignInPopup: function () {
        // e.preventDefault();
        view.hideModal('jsSignInModal', 'jsSignInModalBody');
        $('#modalPopup').addClass('jsSignInModal').addClass('wavesModal').modal('show')
                .find('.jsSignInModalBody').css('display', 'block');
        $('#exampleModalLabel').text('Вход');
    },
    hideModal: function (nameModal, nameSelectBody) {
        app.arrayModalBody.forEach(function (select) {
            if (select !== nameSelectBody) {
                $('.' + select).css('display', 'none');
            }
        });
        app.arrayModal.forEach(function (select) {
            if (select !== nameModal) {
                $('#modalPopup').removeClass(select);
            }
        });
        $('.' + nameSelectBody + ' .errorMessage').text('');
    },
    drawTargetSelect: function (idCategory) {

        var jsSelectTarget = $('.jsSelectTarget');
        var html = '';
        arrayTarget[idCategory].forEach(function (item, index, arr) {
            html += [
                '<option value="' + index + '">' + item + '</option>'
            ].join('\n');
        });
        jsSelectTarget.html(html);
    },
    drawUserPage: function () {
        $('.jsPhoneUser').text(window.app.user.phone).attr('data', window.app.user.phone);
        if (typeof window.app.user.email !== "undefined") {
            $('.jsEmailUser').text(window.app.user.email).attr('data', window.app.user.email);
            $('.jsSetupEmail').attr('display', 'none');
        } else {
            $('.jsEmailUser').text('e-mail еще не задан').attr('data', undefined);
            $('.jsSetupEmail').css('display', 'block');
            $('.jsConfirmEmail').css('display', 'none');
            $('.jsChangeEmail').css('display', 'none');
        }

        // TODO : point work
        // var add = $(".jsSelectCategory").find(":selected").val();

        if (typeof window.app.user.target_name !== "undefined" &&
            typeof window.app.user.target_cat !== "undefined") {

            var idCategory = arrayCategory.indexOf(window.app.user.target_cat);
            var idTarget = arrayTarget[idCategory].indexOf(window.app.user.target_name);
            var jsSelectTarget = $('.jsSelectTarget');

            view.drawTargetSelect(idCategory);
            $(".jsSelectCategory option[value=" + idCategory + "]").attr('selected', 'true');
            $(".jsSelectTarget option[value=" + idTarget + "]").attr('selected', 'true');
        } else {

        }

    },
    showPersonalAreaPopup: function (elem) {
        elem.preventDefault();
        view.hideModal('jsPersonalArea', 'jsChangeEmailModalBody');
        $('#modalPopup').addClass('jsPersonalArea').addClass('personalArea').addClass(
                'wavesModal').modal('show')
                .find('.jsChangeEmailModalBody').css('display', 'block');
        $('#exampleModalLabel').text('Личный кабинет');
        $('.jsPhoneUser').text(window.app.user.phone);
    },
    showChangeMailPopup: function () {
        view.hideModal('jsRegisterModal', 'jsChangeEmailModalBody');
        $('#modalPopup').addClass('jsRegisterModal').addClass('wavesModal').modal('show')
                .find('.jsChangeEmailModalBody').css('display', 'block');
        $('#exampleModalLabel').text('Смена Электронной почты');
    },
    showChangePasswordPopup: function () {
        view.hideModal('jsRegisterModal', 'jsChangePasswordModalBody');
        $('#modalPopup').addClass('jsRegisterModal').addClass('wavesModal').modal('show')
                .find('.jsChangePasswordModalBody').css('display', 'block');
        $('#exampleModalLabel').text('Сменить пароль');
    },
    showChangeLoginPhone: function () {
        view.hideModal('jsChangeLoginPhoneModal', 'jsChangeLoginPhoneModalBody');
        $('#modalPopup').addClass('jsChangeLoginPhoneModal').addClass('wavesModal').modal('show')
                .find('.jsChangeLoginPhoneModalBody').css('display', 'block');
        $('#exampleModalLabel').text('Смена Логина / № Телефона');
        $("#changeLoginPhoneOld").intlTelInput("setNumber", window.app.user.phone).val(
                window.app.user.phone);
        // $("#changeLoginPhoneOld").val(window.app.user.phone);
    },
    showRegisterPopup: function (elem) {
        elem.preventDefault();
        view.hideModal('jsRegisterModal', 'jsRegisterModalBody');
        $('#modalPopup').addClass('jsRegisterModal').addClass('wavesModal').modal('show')
                .find('.jsRegisterModalBody').css('display', 'block');
        $('#exampleModalLabel').text('Регистрация');
    },
    showAccessRecovery: function (e) {
        e.preventDefault();
        view.hideModal('jsAccessRecoveryModal', 'jsAccessRecoveryModalBody');
        $('#modalPopup').addClass('jsAccessRecoveryModal').addClass('wavesModal').modal('show')
                .find('.jsAccessRecoveryModalBody').css('display', 'block');
        $('#exampleModalLabel').text('Восстановление доступа');
    },
    showRecoveryPass: function (e) {
        e.preventDefault();
        view.hideModal('jsRecoveryPassModal', 'jsRecoveryPassModalBody');
        $('#modalPopup').addClass('jsRecoveryPassModal').addClass('wavesModal').modal('show')
                .find('.jsRecoveryPassModalBody').css('display', 'block');
        $('#exampleModalLabel').text('Восстановление доступа');
    },
    showRecoveryPassDone: function (e) {
        $('.jsRecoveryPassModal').modal('hide');
        setTimeout(function () {
            view.hideModal('jsAccessRecoveryPassModal', 'jsAccessRecoveryPassModalBodyDone');
            $('#exampleModalLabelDone').text('Пароль установлен');
            $('#modalPopupDone').addClass('jsAccessRecoveryPassModal').addClass('wavesModal').modal(
                    'show')
                    .find('.jsAccessRecoveryPassModalBodyDone').css('display', 'block');
        }, 500);
    },
    showAccessRecoveryDone: function (e) {
        e.preventDefault();
        $('.jsAccessRecoveryModal').modal('hide');
        setTimeout(function () {
            view.hideModal('jsAccessRecoveryDoneModal', 'jsAccessRecoveryModalBodyDone');
            $('#exampleModalLabelDone').text('Восстановление доступа1');
            $('#modalPopupDone').addClass('jsAccessRecoveryDoneModal').addClass('wavesModal').modal(
                    'show')
                    .find('.jsAccessRecoveryModalBodyDone').css('display', 'block');
        }, 500);
    },
    showRegisterDone: function () {
        $('.jsRegisterModal').modal('hide');
        setTimeout(function () {
            view.hideModal('jsRegisterDoneModal', 'jsRegisterModalBodyDone');
            $('#exampleModalLabelDone').text('Регистрация');
            $('#modalPopupDone').addClass('jsRegisterDoneModal').addClass('wavesModal').modal(
                    'show')
                    .find('.jsRegisterModalBodyDone').css('display', 'block');
        }, 500);
    },
    drawAllWaves: function () {
        var arrayWaves = app.arrayWaysTotals;
        var $blockWaysRow = $('.block-ways-row');
        view.drawWaves(arrayWaves, $blockWaysRow);
    },
    drawSinglWaves: function (arrayWaves, $blockWaysRow) {
        view.drawWaves(arrayWaves, $blockWaysRow);
    },
    drawWaves: function (arrayWaves, $blockWaysRow) {
        arrayWaves.forEach(function (wave, index, array) {
            if (wave >= (app.waveLevelsArray[index][5])) {
                var block = app.arrayWaysType.w6;
                $blockWaysRow.append(block.replace('m%', wave).replace('n%', (index + 1)).replace(
                        'data-id="%"', 'data-id=' + index + ''));
            } else if (wave >= (app.waveLevelsArray[index][4])) {
                var block = app.arrayWaysType.w5;
                $blockWaysRow.append(block.replace('m%', wave).replace('n%', (index + 1)).replace(
                        'data-id="%"', 'data-id=' + index + ''));
            } else if (wave >= (app.waveLevelsArray[index][3])) {
                var block = app.arrayWaysType.w4;
                $blockWaysRow.append(block.replace('m%', wave).replace('n%', (index + 1)).replace(
                        'data-id="%"', 'data-id=' + index + ''));
            } else if (wave >= (app.waveLevelsArray[index][2])) {
                var block = app.arrayWaysType.w3;
                $blockWaysRow.append(block.replace('m%', wave).replace('n%', (index + 1)).replace(
                        'data-id="%"', 'data-id=' + index + ''));
            } else if (wave >= (app.waveLevelsArray[index][1])) {
                var block = app.arrayWaysType.w2;
                $blockWaysRow.append(block.replace('m%', wave).replace('n%', (index + 1)).replace(
                        'data-id="%"', 'data-id=' + index + ''));
            } else if (wave >= 0) {
                var block = app.arrayWaysType.w1;
                $blockWaysRow.append(block.replace('m%', wave).replace('n%', (index + 1)).replace(
                        'data-id="%"', 'data-id=' + index + ''));
            }
        });
    },
    clearSingleWaveInfo: function () {
        $('.jsBlockWaysRowPopup').html('');
    },
    drawListMembers: function (id, array) {
        // app.wavesUsersLists = ['+380999297629', '+387999297629','+3905656565656',
        // '+3809994444629'];
        window.app.wavesUsersLists = array;
        var $blockListMemberPopup = $('.jsBlockListMemberPopup');
        $blockListMemberPopup.html('');
        var n = 0;
        arrayCoef[8] = window.app.user.bonus_factor;
        if (window.app.wavesUsersLists) {
            window.app.wavesUsersLists.forEach(function (element, index, array) {
                n = (n == 5) ? 0 : n;
                $blockListMemberPopup.append('<div class="col-md-6">' + '<div class="handBlock">' +
                                             '<div class="hand" style="background-color:' +
                                             app.arrayHandColor[n] + '";">' +
                                             '<img src="img/hand.png" alt="">' + '</div>' +
                                             '<div class="handPhone">' + element.phone + '</div>' +
                                             '</div>' + '</div>');
                n++;
            });
        } else {
            $blockListMemberPopup.html('<h2 style="text-align: center">Список учасников пуст</h2>');
        }

        $('.jsBlockWaysRowPopup').find('.number_way').text((parseInt(id) + 1) + '~');
        $('#jsTitlePopupSinglWave').text(parseInt(id) + 1);
        $('.jsN-members').text(window.app.arrayWaysTotals[id]);
        $('.jsN-bonusFactor').text(arrayCoef[id]);
        $('.jsSumm').text(window.app.arrayWaysTotals[id] * arrayCoef[id]);

    },
    showTooltipCopyUrlReferral: function (e) {
        e.preventDefault();
        var $jsReferralLink = $(e.target).closest('.jsReferralLink');
        var referralLink = $jsReferralLink.attr('data-ref');
        var aHref = $jsReferralLink.attr('href');

        clipboard.copy(referralLink).then(function () {
            $('.jsReferralLink').tooltip({
                title: "Ссылка на реферальную программу скопирована в буфер!",
                trigger: "click, focus"
            });
            console.log("success");
            setTimeout(function () {
                console.log('click');
                window.open(aHref, '_blank');
            }, 1000);
        }, function (err) {
            console.log("failure", err);
        });
    },
    drawTask: function (taskIndex) {
        if (taskIndex !== "undefined") {
            $('#title-task').text(window.app.user.taskList[taskIndex].title).attr('data-task',
                    taskIndex);
            $('#text-task').html(window.app.user.taskList[taskIndex].text);
            if (taskIndex) {
                $('.jsBtnPrev').css('display', 'block');
            } else {
                $('.jsBtnPrev').css('display', 'none');
            }
            if (taskIndex == 6) {
                $('.jsTaskBtnDone').css('display', 'none');
            } else {
                $('.jsTaskBtnDone').css('display', 'inline-block');
            }
        } else {
            $('#title-task').text('Задание еще не добавленно.');
            $('#text-task').html('Текст задания еще не добавлен.');
        }
    },
    creatReferralBtn: function () {

        var dataRef = 'http://onwave.su/?ref=' + $.cookie('user_phone');
        var referralFb = "https://www.facebook.com/sharer/sharer.php?u=https://onwave.su/?ref=" +
                         dataRef;
        var referralVk = "https://vk.com/share.php?url=https://onwave.su/?ref=" + dataRef;
        var referralOk = "http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1&st._surl=" +
                         dataRef;

        $('.jsReferralFb').attr('href', referralFb).attr('data-ref', dataRef);
        $('.jsReferralVk').attr('href', referralVk).attr('data-ref', dataRef);
        $('.jsReferralOk').attr('href', referralOk).attr('data-ref', dataRef);

    },
    setActiveUrl: function (id) {
        var $ul = $('.menu-block li');
        $ul.each(function (index, item) {
            if (id === index) {
                $(item).addClass('active');
            }
        });
    },
    showSubMenu: function (e) {
        $('.jsSubMenu').show();
    }
};

var model = {
    getUserModelById: function (callback) {
        $.ajax({
            url: 'http://82.146.58.19:3000/api/v1/getuserbyid',
            type: 'POST',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Authorization": $.cookie('user_api_token')
            },
            data: {
                id: $.cookie('user_id')
            },
            statusCode: {
                401: function (xhr) {
                    console.log('Unauthorized');
                    window.alert(xhr.responseText);
                },
                422: function (xhr) {
                    console.log('User not found');
                    window.alert(xhr.responseText);
                }
            },
            success: function (data) {
                app.user = data;
                view.loginUserData(data);
                app.arrayWaysTotals = [
                    app.user.w1,
                    app.user.w2,
                    app.user.w3,
                    app.user.w4,
                    app.user.w5,
                    app.user.w6,
                    app.user.w7,
                    app.user.w8,
                    app.user.w9
                ];
                window.app = app;

                callback();
            },
            error: function () {
                window.alert('An error occurred! Please check your internet connection and try ' +
                             'again. If this error persists please contact support');
            }
        });
    },
    getSendSMS: function (data, callback) {
        $.ajax({
            url: 'http://82.146.58.19:3000/api/v1/user',
            type: 'POST',
            header: "Access-Control-Allow-Origin: *",
            data: {
                phone: data.phone,
            },
            statusCode: {
                400: function (xhr) {
                    window.alert(xhr.responseText);
                },
                403: function (xhr) {
                    window.alert(xhr.responseText);
                }
            },
            success: function (_data) {
                $('.errorMessage').text('SMS код  отправлен на ваш телефон').css('visibility',
                        'visible');

                if (data.form === 'register') {
                    $('.jsInputSMSRegister').removeAttr('disabled');
                    $('.jsBtnRegisterAction').removeAttr('disabled');
                } else {
                    $('.jsInputSMS').removeAttr('disabled');
                    $('.jsBtnLoginAction').removeAttr('disabled');
                }
                callback();
            },
            error: function () {
                window.alert('An error occurred! Please check your internet connection and try ' +
                             'again. If this error persists please contact support');
            }
        });
    },
    getAuthorizedUserData: function (data, callback) {
        $.ajax({
            url: 'http://82.146.58.19:3000/api/v1/user',
            type: 'PATCH',
            header: "Access-Control-Allow-Origin: *",
            data: {
                phone: data.phone,
                pin: data.sms
            },
            statusCode: {
                403: function (xhr) {
                    callback({error: xhr.responseText});
                },
                404: function (xhr) {
                    callback({error: xhr.responseText});
                },
                422: function (xhr) {
                    callback({error: xhr.responseText});
                }

            },
            success: function (data) {
                window.app.user = data;
                $.cookie('user_api_token', window.app.user.token);
                $.cookie('pin', window.app.user.pin);
                $.cookie('user_id', window.app.user._id);
                $.cookie('user_phone', window.app.user.phone);
                // $('.jsSignInModal').modal('hide');
                // view.loginUserData(app.user);
                // window.location.href = '/';
                callback({
                    error: false,
                    data
                });
            },
            error: function () {
                console.log('An error occurred! Please check your internet connection and try ' +
                            'again. If this error persists please contact support');
            }
        });
    },
    saveUserEmail: function (data, callback) {
        $.ajax({
            url: 'http://82.146.58.19:3000/api/v1/setUserEmail',
            type: 'PATCH',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Authorization": $.cookie('user_api_token')
            },
            data: {
                email: data.email,
            },
            statusCode: {
                400: function (xhr) {
                    window.alert(xhr.responseText);
                },
                403: function (xhr) {
                    window.alert(xhr.responseText);
                }
            },
            success: function (data) {
                callback();
            },
            error: function () {
                window.alert('An error occurred! Please check your internet connection and try ' +
                             'again. If this error persists please contact support');
            }
        });
    },
    saveUserSetting: function (data, callback) {
        $.ajax({
            url: 'http://82.146.58.19:3000/api/v1/user',
            type: 'POST',
            header: "Access-Control-Allow-Origin: *",
            data: {
                phone: data.phone,
                email: data.email,
                password: data.password,
            },
            statusCode: {
                400: function (xhr) {
                    window.alert(xhr.responseText);
                },
                403: function (xhr) {
                    window.alert(xhr.responseText);
                }
            },
            success: function (data) {
                $.cookie('user_api_token', app.user.pin);
                $.cookie('user_id', app.user._id);
                $.cookie('user_phone', app.user.phone);

                callback();
            },
            error: function () {
                window.alert('An error occurred! Please check your internet connection and try ' +
                             'again. If this error persists please contact support');
            }
        });
    },
    setUserPhonePatch: function (data, callback) {
        $.ajax({
            url: 'http://82.146.58.19:3000/api/v1/setUserPhone',
            type: 'PATCH',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Authorization": $.cookie('user_api_token')
            },
            data: {
                pin: data.pin,
            },
            statusCode: {
                401: function (xhr) {
                    callback('Invalid credentials');
                    // window.alert(xhr.responseText);
                },
                402: function (xhr) {
                    callback('Wrong pin');
                    // window.alert(xhr.responseText);
                },
                422: function (xhr) {
                    callback('User not found');
                    // window.alert(xhr.responseText);
                }
            },
            success: function (data) {
                callback('Update success');
            },
            error: function () {
                console.log('An error occurred! Please check your internet connection and try ' +
                            'again. If this error persists please contact support');
            }
        });
    },
    setUserPhone: function (data, callback) {
        $.ajax({
            url: 'http://82.146.58.19:3000/api/v1/setUserPhone',
            type: 'POST',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Authorization": $.cookie('user_api_token')
            },
            data: {
                phone: data.phone,
            },
            statusCode: {
                401: function (xhr) {
                    callback('Invalid credentials');
                    // window.alert(xhr.responseText);
                },
                402: function (xhr) {
                    callback('User already exists');
                    // window.alert(xhr.responseText);
                },
                422: function (xhr) {
                    callback('User not found');
                    // window.alert(xhr.responseText);
                }
            },
            success: function (data) {
                callback('Pin send sucessfully');
            },
            error: function () {
                console.log('An error occurred! Please check your internet connection and try ' +
                            'again. If this error persists please contact support');
            }
        });
    },
    saveUserPass: function (data, callback) {
        $.ajax({
            url: 'http://82.146.58.19:3000/api/v1/setUserPassword',
            type: 'PATCH',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Authorization": $.cookie('user_api_token')
            },
            data: {
                password: data.password,
            },
            statusCode: {
                400: function (xhr) {
                    window.alert(xhr.responseText);
                },
                403: function (xhr) {
                    window.alert(xhr.responseText);
                }
            },
            success: function (data) {
                callback();
            },
            error: function () {
                window.alert('An error occurred! Please check your internet connection and try ' +
                             'again. If this error persists please contact support');
            }
        });
    },
    registerUser: function (data, callback) {
        $.ajax({
            url: 'http://82.146.58.19:3000/api/v1/user',
            type: 'POST',
            header: "Access-Control-Allow-Origin: *",
            data: {
                phone: data.phone,
                email: "",
                password: data.password,
                upper_user_phone: (typeof window.app.referral !== "undefined") ?
                                  window.app.referral : ''
            },
            statusCode: {
                400: function (xhr) {
                    window.alert(xhr.responseText);
                },
                403: function (xhr) {
                    window.alert(xhr.responseText);
                }
            },
            success: function (data) {
                // app.user = data;
                // $.cookie('user_api_token', app.user.pin);
                // $.cookie('user_id', app.user._id);
                // $.cookie('user_phone', app.user.phone);

                $('.jsSignInModal').modal('hide');
                // view.loginUserData(app.user);
                view.showRegisterDone();
                callback();
            },
            error: function () {
                window.alert('An error occurred! Please check your internet connection and try ' +
                             'again. If this error persists please contact support');
            }
        });
    },
    registerUserPhone: function (_data, callback) {
        $.ajax({
            url: 'http://82.146.58.19:3000/api/v1/user',
            type: 'PATCH',
            header: "Access-Control-Allow-Origin: *",
            data: {
                phone: _data.phone,
                pin: _data.pin
            },
            statusCode: {
                403: function (xhr) {
                    callback({
                        error: 'Emty body',
                        data: _data
                    });
                    // window.alert(xhr.responseText);
                },
                404: function (xhr) {
                    callback({
                        error: 'User not found',
                        data: _data
                    });
                    // window.alert(xhr.responseText);

                },
                422: function (xhr) {
                    callback({
                        error: 'Invalid or overdue pin',
                        data: _data
                    });
                    // window.alert(xhr.responseText);
                }
            },
            success: function (data) {
                window.app.user = data;
                $.cookie('user_api_token', app.user.token);
                $.cookie('pin', app.user.pin);
                $.cookie('user_id', app.user._id);
                $.cookie('user_phone', app.user.phone);
                callback({data: _data});
            },
            error: function () {
                console.log('An error occurred! Please check your internet connection and try ' +
                            'again. If this error persists please contact support');
            }
        });
    },
    getWavesUsersLists: function (wave_id, callback) {
        $.ajax({
            // url: 'http://82.146.58.19:3000/api/v1/getWavesUsersLists',
            url: 'http://82.146.58.19:3000/api/v1/getSingleWaveUser',
            type: 'GET',
            data: {
                n_wave: wave_id+1
            },
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Authorization": $.cookie('user_api_token')
            },
            statusCode: {
                400: function (xhr) {
                    window.alert(xhr.responseText);
                },
                403: function (xhr) {
                    window.alert(xhr.responseText);
                }
            },
            success: function (data) {
                app.wavesUsersLists = data;
                callback(app.wavesUsersLists);
            },
            error: function () {
                window.alert('An error occurred! Please check your internet connection and try ' +
                             'again. If this error persists please contact support');
            }
        });
    },
    getTaskList: function (callback) {
        $.ajax({
            url: 'http://82.146.58.19:3000/api/v1/getTaskList',
            type: 'GET',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Authorization": $.cookie('user_api_token')
            },
            statusCode: {
                400: function (xhr) {
                    window.alert(xhr.responseText);
                },
                403: function (xhr) {
                    window.alert(xhr.responseText);
                }
            },
            success: function (data) {
                window.app.user.taskList = data;
                callback(window.app.user.current_task);
            },
            error: function () {
                window.alert('An error occurred! Please check your internet connection and try ' +
                             'again. If this error persists please contact support');
            }
        });
    },
    setUserCurrentTask: function (data, callback) {
        $.ajax({
            url: 'http://82.146.58.19:3000/api/v1/setUserCurrentTask',
            type: 'PATCH',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Authorization": $.cookie('user_api_token')
            },
            data: {
                task_id: data.task_id,
            },
            statusCode: {
                400: function (xhr) {
                    window.alert(xhr.responseText);
                },
                401: function (xhr) {
                    window.alert(xhr.responseText);
                },
                403: function (xhr) {
                    window.alert(xhr.responseText);
                }
            },
            success: function (data) {
                callback();
            },
            error: function () {
                window.alert('An error occurred! Please check your internet connection and try ' +
                             'again. If this error persists please contact support');
            }
        });
    },
    setUserTarget: function (data, callback) {
        $.ajax({
            url: 'http://82.146.58.19:3000/api/v1/setUserTarget',
            type: 'POST',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Authorization": $.cookie('user_api_token')
            },
            data: {
                target_name: data.target_name,
                target_cat: data.target_cat,
            },
            statusCode: {
                400: function (xhr) {
                    window.alert(xhr.responseText);
                },
                401: function (xhr) {
                    window.alert(xhr.responseText);
                },
                403: function (xhr) {
                    window.alert(xhr.responseText);
                }
            },
            success: function (data) {
                callback();
            },
            error: function () {
                window.alert('An error occurred! Please check your internet connection and try ' +
                             'again. If this error persists please contact support');
            }
        });
    },
    getUserRegister: function (data, callback) {
        $.ajax({
            url: 'http://82.146.58.19:3000/api/v1/getUserRegister',
            type: 'GET',
            headers: {
                "Access-Control-Allow-Origin": "*",
            },
            data: {
                phone: data.phone,
            },
            statusCode: {
                400: function (xhr) {
                    callback('unregistered');
                    // window.alert(xhr.responseText);
                },
                401: function (xhr) {
                    callback('invalid credentials');
                    // window.alert(xhr.responseText);
                }
            },
            success: function (data) {
                callback('registered');
            },
            error: function () {
                console.log('An error occurred! Please check your internet connection and try ' +
                            'again. If this error persists please contact support');
            }
        });
    },
    getAuthorizedUserReg: function (data, callback) {
        $.ajax({
            url: 'http://82.146.58.19:3000/api/v1/login_pas',
            type: 'PATCH',
            header: "Access-Control-Allow-Origin: *",
            data: {
                phone: data.phone,
                pin: data.pin,
                upper_user_phone: window.app.referral
            },
            statusCode: {
                400: function (xhr) {
                    callback(xhr);
                    // window.alert(xhr.responseText);
                },
                403: function (xhr) {
                    callback(xhr);
                    // window.alert(xhr.responseText);
                },
                422: function (xhr) {
                    callback(xhr);
                    // window.alert(xhr.responseText);
                }
            },
            success: function (data) {
                window.app.user = data;
                $.cookie('user_api_token', app.user.token);
                $.cookie('pin', app.user.pin);
                $.cookie('user_id', app.user._id);
                $.cookie('user_phone', app.user.phone);
                callback();
                // $('.jsSignInModal').modal('hide');
                // view.loginUserData(app.user);
                // window.location.href = '/';
            },
            error: function () {
                console.log('An error occurred! Please check your internet connection and try ' +
                            'again. If this error persists please contact support');
            }
        });
    },
    authhenticateLogAndPass: function (data, callback) {
        $.ajax({
            url: 'http://82.146.58.19:3000/api/v1/login_pas',
            type: 'PATCH',
            header: "Access-Control-Allow-Origin: *",
            data: {
                phone: data.phone,
                password: data.password,
            },
            statusCode: {
                401: function (xhr) {
                    callback('No password');
                    // window.alert(xhr.responseText);
                },
                403: function (xhr) {
                    callback('Emty body');
                    // window.alert(xhr.responseText);
                },
                404: function (xhr) {
                    callback('User not found');
                    // window.alert(xhr.responseText);
                },
                422: function (xhr) {
                    callback('Invalid or overdue password');
                    // window.alert(xhr.responseText);
                }
            },
            success: function (data) {
                window.app.user = data;
                $.cookie('user_api_token', app.user.token);
                $.cookie('pin', app.user.pin);
                $.cookie('user_id', app.user._id);
                $.cookie('user_phone', app.user.phone);
                $.cookie('user_password', app.user.password);
                callback();
                $('.jsSignInModal').modal('hide');
                view.loginUserData(app.user);
                window.location.href = '/';
            },
            error: function () {
                console.log('An error occurred! Please check your internet connection and try ' +
                            'again. If this error persists please contact support');
            }
        });
    },
    restore_password_by_email: function (data, callback) {
        $.ajax({
            url: 'http://82.146.58.19:3000/api/v1/restore_password_by_email',
            type: 'GET',
            headers: {
                "Access-Control-Allow-Origin": "*",
            },
            data: {
                email: data.email,
            },
            statusCode: {
                400: function (xhr) {
                    callback('email not found');
                    // window.alert(xhr.responseText);
                },
                401: function (xhr) {
                    callback('invalid credentials');
                    // window.alert(xhr.responseText);
                }
            },
            success: function (data) {
                callback('email send successfully');
            },
            error: function () {
                console.log('An error occurred! Please check your internet connection and try ' +
                            'again. If this error persists please contact support');
            }
        });
    }

};

var controller = {
    validationEmail: function (email) {
        var validation = new RegExp(/.+?\@.+/g);
        return validation.test(email);
    },
    pressClickBtnRegisterGetSMS: function (e) {
        e.preventDefault();
        var data = {};
        var codeCountry = $("#loginPhone").siblings(".flag-container").find(".active").attr(
                "data-dial-code");
        data.phone = $('#InputLoginRegister').val();
        data.form = 'register';

        model.getUserRegister(data, function (response) {
            if (response == 'registered') {
                $('.errorMessage')
                        .html('Вы уже зарегистрированы. <a class="jsBtnLogin linkLogin"  ' +
                              'onclick="view.showSignInPopup()" data-toggle="modal" ' +
                              'data-target="#exampleModal">Войти</a>').css('visibility', 'visible');
                return;
            } else if (response == 'invalid credentials') {
                $('.errorMessage').text('Ошибка сервера').css('visibility', 'visible');
                return;
            }
            $('.jsBtnRegisterSmsOk').removeAttr('disabled');
            model.getSendSMS(data, function () {
                view.buttonTimer($(e.target), function () {
                });
            });
        });

    },
    pressClickBtnRecoveryPassGetSMS: function (e) {
        e.preventDefault();
        var data = {};
        var codeCountry = $("#loginPhone").siblings(".flag-container").find(".active").attr(
                "data-dial-code");
        data.phone = $('#inputLoginRecoveryPass').val();
        data.form = 'register';

        model.getUserRegister(data, function (response) {
            if (response == 'unregistered') {
                $('.errorMessage')
                        .html('Этот телефон не зарегистрирован. <a class="linkLogin"  ' +
                              'onclick="view.showRegisterPopup(event)" data-toggle="modal" ' +
                              'data-target="#exampleModal">Регистрация</a>').css('visibility',
                        'visible');
                return;
            } else if (response == 'invalid credentials') {
                $('.errorMessage').text('Ошибка сервера').css('visibility', 'visible');
                return;
            }
            $('#jsInputSMSRecoveryPass').removeAttr('disabled');
            $('.jsBtnRegisterSmsOk').removeAttr('disabled');
            model.getSendSMS(data, function () {
                $('.jsBtnRecoveryPassSmsOk').removeAttr('disabled');
                $('#inputLoginRecoveryPass').attr('disabled', true);
                view.buttonTimer($(e.target), function () {
                });
            });
        });

    },
    pressClickBtnGetSMS: function (e) {
        e.preventDefault();
        var data = {};
        var codeCountry = $("#loginPhone").siblings(".flag-container").find(".active").attr(
                "data-dial-code");
        data.phone = $('#loginPhone').val();

        model.getSendSMS(data);
    },
    pressClickBtnChangeLoginPhoneGetSMS: function (e) {
        e.preventDefault();
        var data = {};
        var oldPhone = $('#changeLoginPhoneOld').val();
        var newPhone = $('#changeLoginPhone').val();
        // var pass = $('#inputPasswordChange').val();

        if (oldPhone !== window.app.user.phone) {
            $('.jsChangeLoginPhoneModal .errorMessage').text(
                    'Вы ввели не правильно текущий телефон').css('visibility', 'visible');
            return;
        }
        if (newPhone.length < 6) {
            $('.jsChangeLoginPhoneModal .errorMessage').text('Телефон введен не верно').css(
                    'visibility', 'visible');
            return;
        }
        // if (pass.length < 4) {
        //     $('.errorMessage').text('Пароль должен быть не мение 4 букв').css('visibility',
        //             'visible');
        //     return;
        // }

        data.phone = newPhone;

        model.setUserPhone(data, function (response) {
            var errorMessage = $('.jsChangeLoginPhoneModal .errorMessage');

            switch (response) {
                case 'Pin send sucessfully':
                    window.app.user.phone = data.phone;
                    errorMessage.html('SMS код  отправлен на ваш новый телефон.').show();
                    $('.jsBtnAccessChangeLogisPhoneAction').removeAttr('disabled');
                    $(' .jsChangeLoginPhoneModalBody .jsInputSMS').removeAttr('disabled');
                    $('#changeLoginPhoneOld').attr('disabled', true);
                    $('#changeLoginPhone').attr('disabled', true);
                    break;
                case 'User not found':
                    errorMessage.html('Пользователь не найден.').show();
                    break;
                case 'User already exists':
                    errorMessage.html('Пользователь уже существует.').show();
                    break;
                case 'Invalid credentials':
                    errorMessage.html('Недопустимые учетные данные.').show();
                    break;
                default:
                    break;
            }

            view.buttonTimer($(e.target), function () {
            });

        });
    },
    pressClickBtnLoginPassAction: function (e) {
        e.preventDefault();
        var data = {};
        data.phone = $('#loginPhone').val();
        data.password = $('#InputPasswordLogin').val();

        if (!data.password) {
            var errorMessage = $('.jsSignInModalBody .errorMessage');
            errorMessage.html('Введите пароль.').show();
            return;
        }
        model.authhenticateLogAndPass(data, function (response) {
            var errorMessage = $('.jsSignInModalBody .errorMessage');
            var forgotPassBlock = $('.jsForgotPass');
            switch (response) {
                case 'No password':
                    errorMessage.html('Пароль введен не верно.').show();
                    forgotPassBlock.show();
                    break;
                case 'Emty body':
                    errorMessage.html('Данный телефон не зарегистрирован.  ' +
                                      '<br><a onclick="view.showRegisterPopup(event);" href="">Регистрация</a>').show();
                    break;
                case 'User not found':
                    errorMessage.html('Данный телефон не зарегистрирован.  ' +
                                      '<br><a onclick="view.showRegisterPopup(event);" href="">Регистрация</a>').show();
                    break;
                case 'Invalid or overdue password':
                    errorMessage.html('Пароль введен не верно.').show();
                    forgotPassBlock.show();
                    break;
                default:
                    break;
            }
        });
    },
    pressClickBtnLoginAction: function (e) {
        e.preventDefault();
        var data = {};
        data.phone = $('#loginPhone').val();
        data.sms = $('.jsInputSMS').val();
        model.getAuthorizedUserData(data, function () {
            $('.jsSignInModal').modal('hide');
            view.loginUserData(window.app.user);
            window.location.href = '/';
        });
    },
    pressClickBtnAction_done: function (e) {
        e.preventDefault();
        $('#modalPopupDone').modal('hide');
    },
    pressClickBtnExit: function (e) {
        e.preventDefault();
        $.cookie('user_api_token', null);
        $.cookie('pin', null);
        $.cookie('user_id', null);
        $.cookie('user_phone', null);
        window.location.href = '/';
    },
    pressClickBtnRegisterAction: function (e) {
        e.preventDefault();
        var pass = $('#InputPasswordRegister').val();
        var pass2 = $('#InputRePasswordRegister2').val();

        if (pass !== pass2) {
            $('.errorMessage2').text('Пароли не совпадают').css('visibility', 'visible');
            return;
        }
        if ((pass.length < 3) || (pass2.length < 3)) {
            $('.errorMessage2').text('Слишком короткий пароль').css('visibility', 'visible');
            return;
        }
        // else if (!controller.validationEmail(email)) {
        //     $('.errorMessage').text('Не верный email').css('visibility', 'visible');
        //     return;
        // }
        var data = {};
        data.password = pass;
        model.saveUserPass(data, function () {
            view.showRegisterDone();
            model.getUserModelById(function () {
                window.location.href = '/user.html';
            });
        });
    },
    pressClickBtnRecoveryPassAction: function (e) {
        e.preventDefault();
        var pass = $('#InputPasswordRecoveryPass').val();
        var pass2 = $('#InputRePasswordRecoveryPass2').val();

        if (pass !== pass2) {
            $('.errorMessage2').text('Пароли не совпадают').css('visibility', 'visible');
            return;
        }
        if ((pass.length < 3) || (pass2.length < 3)) {
            $('.errorMessage2').text('Слишком короткий пароль').css('visibility', 'visible');
            return;
        }
        // else if (!controller.validationEmail(email)) {
        //     $('.errorMessage').text('Не верный email').css('visibility', 'visible');
        //     return;
        // }
        var data = {};
        data.password = pass;
        model.saveUserPass(data, function () {
            view.showRecoveryPassDone();
            // model.getUserModelById(function () {
            //     window.location.href = '/user.html';
            // });
        });
    },
    pressClickSaveUserSetting: function (e) {
        e.preventDefault();
        var data = {};
        data.phone = $('.jsPhoneUser').attr('data');
        data.email = $('.jsEmailUser').attr('data');
        data.target_cat = $('.jsSelectCategory').find(":selected").text();
        data.target_name = $('.jsSelectTarget').find(":selected").text();
        data.password = window.app.user.password;
        // model.saveUserSetting(data, function () {
        model.setUserTarget(data, function () {
        });
        // });
    },
    singleWave: function (e) {
        e.preventDefault();
        var id = +$(e.target).closest('.jsWave').attr('data-id');

        model.getWavesUsersLists(id, function (data) {
            view.drawListMembers(id, data);
        });
        $('#modalPopupSinglWave').modal('show');

        view.drawSinglWaves([app.arrayWaysTotals[id]], $('.jsBlockWaysRowPopup'));

    },
    selectCountry: function (e) {
        var intlTelInput = $(e.target).closest('.intl-tel-input');
        var code = intlTelInput.find('input').siblings(".flag-container").find(".active").attr(
                "data-dial-code");
        intlTelInput.find('input').val('+' + code);
    },
    pressClickChangeEmail: function (e) {
        e.preventDefault();
        view.showChangeMailPopup();

    },
    pressClickChangePassword: function (e) {
        e.preventDefault();
        view.showChangePasswordPopup();

    },
    pressClickBtnChangeEmailActive: function (e) {
        e.preventDefault();
        var data = {};
        data.email = $('#inputEmailChangeRecovery').val();
        // data.phone = window.app.user.phone;
        // data.password = (typeof data.email !== "undefined") ? data.email : '';
        if (controller.validationEmail(data.email)) {
            model.saveUserEmail(data, function () {
                window.app.user.email = data.email;
                $('.jsEmailUser').text(data.email).attr('data-email', data.email);
                $('.jsRegisterModal').modal('hide');
                setTimeout(function () {
                    view.hideModal('jsRegisterModal', 'jsChangeEmailModalBodyDone');
                    $('#exampleModalLabelDone').text('Смена ЭЛЕКТРОННОЙ ПОЧТЫ');
                    $('#modalPopupDone').addClass('jsChangeEmailModalBodyDone').addClass(
                            'wavesModal').modal('show')
                            .find('.jsChangeEmailModalBodyDone').css('display', 'block');
                }, 500);

            });
        } else {
            $('.errorMessage').text('Не верный email').css('visibility', 'visible');
            return;
        }
    },
    pressClickBtnChangePasActive: function (e) {
        e.preventDefault();
        var data = {};
        data.passwordOld = $('#inputPasswordChangeRecoveryOld').val();
        data.password = $('#inputPasswordChangeRecovery').val();
        data.rePassword = $('#inputRePasswordChangeRecovery').val();
        if (data.password !== data.rePassword) {
            $('.errorMessage').text('пароли не совпадают').css('visibility', 'visible');
            return;
        }
        if (data.passwordOld.length < 4 || data.password.length < 4 || data.rePassword.length < 4) {
            $('.errorMessage').text('Пароль должен быть не мение 4 букв').css('visibility',
                    'visible');
            return;
        }

        model.saveUserPass(data, function () {
            $('.jsRegisterModal').modal('hide');
            setTimeout(function () {
                view.hideModal('jsRegisterModal', 'jsChangePasswordModalBodyDone');
                $('#exampleModalLabelDone').text('Смена пароля');
                $('#modalPopupDone').addClass('jsChangePasswordModalBodyDone').addClass(
                        'wavesModal').modal('show')
                        .find('.jsChangePasswordModalBodyDone').css('display', 'block');
            }, 500);

        });

    },
    pressClickBtnChangeLoginPhoneActive: function (e) {
        e.preventDefault();
        var data = {};

        data.phone = $('#changeLoginPhone').val();
        data.pin = $('.jsChangeLoginPhoneModalBody .jsBtnGeLogintSMS').val();

        model.setUserPhonePatch(data, function (response) {
            var errorMessage = $('.jsChangeLoginPhoneModalBody .errorMessage');
            switch (response) {
                case 'Update success':
                    $('.jsChangeLoginPhoneModal').modal('hide');
                    $('.jsPhoneUser').text(data.phone);
                    window.app.user.phone = data.phone;
                    break;
                case 'Invalid credentials':
                    errorMessage.html('Недопустимые учетные данные.').show();
                    break;
                case 'Wrong pin':
                    errorMessage.html('Неправильный sms код').show();
                    break;
                case 'User not found':
                    errorMessage.html('Пользователь не найдет').show();
                    break;
                default:
                    break;
            }
        });
    },
    pressClickBtnChangeLoginPhone: function (e) {
        e.preventDefault();
        view.showChangeLoginPhone();
    },
    pressClickTaskDone: function (e) {
        e.preventDefault();
        var data = {};
        var id = parseInt($('#title-task').attr('data-task'));
        id += 1;
        data.task_id = window.app.user.taskList[id]._id;

        model.setUserCurrentTask(data, function () {
            app.user.current_task = data.task_id;
            view.drawTask(parseInt(id));

        });
    },
    changeSelectCategory: function (e) {
        var idCategory = $(".jsSelectCategory").find(":selected").val();
        view.drawTargetSelect(idCategory);
    },
    pressClickBtnRegisterSMSOk: function (e) {
        e.preventDefault();
        var data = {};
        data.phone = $('#InputLoginRegister').val();
        data.pin = $('#jsInputSMSRegister').val();

        model.registerUserPhone(data, function (data) {
            if (data.error) {
                $('.errorMessage').text('Вы ввели неправильный код.').css('visibility', 'visible');
                return;
            }
            $('.errorMessage').text(
                    'Телефон подтвержден. Он является вашим логином. Задайте пожалуйста пароль').css(
                    'visibility', 'visible');
            $('#InputPasswordRegister').removeAttr('disabled');
            $('#InputRePasswordRegister2').removeAttr('disabled');
            $('.jsBtnRegisterAction').removeAttr('disabled');
            $('#jsInputSMSRegister').attr('disabled', true);
            $('#InputLoginRegister').attr('disabled', true);
            // model.getAuthorizedUserReg(data, function (err) {
            //     if (err) {
            //         $('.errorMessage').text('Вы ввели неправильный код.').css('visibility',
            //                 'visible');
            //         return;
            //     }
            //     $('.errorMessage').text(
            //             'Телефон подтвержден. Он является вашим логином. Задайте пожалуйста
            // пароль').css( 'visibility', 'visible');
            // $('#InputPasswordRegister').removeAttr('disabled');
            // $('#InputRePasswordRegister2').removeAttr('disabled');
            // $('#jsInputSMSRegister').attr('disabled', true);
            // $('#InputLoginRegister').attr('disabled', true); });
        });
    },
    pressClickBtnReloadPassSMSOk: function (e) {
        e.preventDefault();
        var data = {};
        data.phone = $('#inputLoginRecoveryPass').val();
        data.sms = $('#jsInputSMSRecoveryPass').val();

        // e.preventDefault();
        // var data = {};
        // data.phone = $('#loginPhone').val();
        // data.sms = $('.jsInputSMS').val();
        model.getAuthorizedUserData(data, function (data) {

            var errorMessage3 = $('.errorMessage3');
            if (data.error) {
                errorMessage3.text('Вы ввели неправильный код.').css('visibility', 'visible');
                return;
            }
            errorMessage3.text('Задайте новый пароль').css('visibility', 'visible');

            $('#InputPasswordRecoveryPass').removeAttr('disabled');
            $('#InputRePasswordRecoveryPass2').removeAttr('disabled');
            $('.jsBtnRecoveryPassAction').removeAttr('disabled');
        });

        // model.registerUserPhone(data, function (data) {
        //     if (data.error) {
        //         $('.errorMessage').text('Вы ввели неправильный код.').css('visibility',
        //                 'visible');
        //         return;
        //     }
        //     $('.errorMessage').text(
        //             'Телефон подтвержден. Он является вашим логином. Задайте пожалуйста
        // пароль').css( 'visibility', 'visible');
        // $('#InputPasswordRegister').removeAttr('disabled');
        // $('#InputRePasswordRegister2').removeAttr('disabled');
        // $('#jsInputSMSRegister').attr('disabled', true);
        // $('#InputLoginRegister').attr('disabled', true); // model.getAuthorizedUserReg(data,
        // function (err) { //     if (err) { //         $('.errorMessage').text('Вы ввели
        // неправильный код.').css('visibility', //                 'visible'); //         return;
        // //     } //     $('.errorMessage').text( //             'Телефон подтвержден. Он
        // является вашим логином. Задайте пожалуйста пароль').css( //             'visibility',
        // 'visible'); //     $('#InputPasswordRegister').removeAttr('disabled'); //
        // $('#InputRePasswordRegister2').removeAttr('disabled'); //
        // $('#jsInputSMSRegister').attr('disabled', true); //
        // $('#InputLoginRegister').attr('disabled', true); // }); });
    },
    setupEmail: function (e) {
        e.preventDefault();
        $('.jsEmailUser').hide();
        $('.jsInputSetupEmail').removeClass('hidden');
    },
    setupEmailOfInput: function (e) {
        e.preventDefault();
        var data = {};
        data.email = $('.jsInputEmail').val();
        if (controller.validationEmail(data.email)) {
            $('.jsInputEmail').css('border', '0px solid red');
            model.saveUserEmail(data, function () {
                window.app.user.email = data.email;
                $('.jsEmailUser').text(data.email).attr('data-email', data.email).show();
                $('.jsInputSetupEmail').hide();
            });
        } else {
            $('.jsInputEmail').css('border', '1px solid red');
            // $('.errorMessageMain').text('Не верный email').css('visibility', 'visible');
            return;
        }
    },
    pressClickBtnRecoveryPassEmail: function (e) {
        e.preventDefault();
        var data = {};
        data.email = $('#InputEmailAccessRecovery').val();
        model.restore_password_by_email(data, function (response) {
            var errorMessage = $('.jsAccessRecoveryModalBody .errorMessage');
            switch (response) {
                case 'email send successfully':
                    errorMessage.html('Новый пароль отправлен на почту..').show();
                    // view.showAccessRecoveryDone();
                    break;
                case 'email not found':
                    errorMessage.html('Данная почта не зарегистрирована.' +
                                      '<br><a onclick="view.showRegisterPopup(event);" href="">Попробуйте восстановить/ задать пароль через смс.</a>').show();
                    break;
                case 'invalid credentials':
                    errorMessage.html('Ошибка сервера...').show();
                    break;
                default:
                    break;
            }
        });

    }

};

window.app = {
    arrayHandColor: [
        '#ec82fb',
        'white',
        '#697bec',
        '#75ec69',
        '#ecda69'
    ],
    arrayWaysTotals: [],
    waveLevelsArray: [
        [
            1,
            2,
            3,
            4,
            5,
            6
        ],
        [
            1,
            6,
            11,
            16,
            21,
            25
        ],
        [
            1,
            26,
            51,
            76,
            101,
            125
        ],
        [
            1,
            126,
            257,
            376,
            501,
            625
        ],
        [
            1,
            626,
            1251,
            1876,
            2501,
            3125
        ],
        [
            1,
            1326,
            6251,
            9376,
            12501,
            15625
        ],
        [
            1,
            15626,
            31251,
            46876,
            62501,
            78125
        ],
        [
            1,
            78126,
            156251,
            234376,
            312501,
            39625
        ],
        [
            1,
            390626,
            781251,
            1171876,
            1562501,
            1953125
        ]
    ],
    arrayWaysType: {
        w1: '<div data-id="%" class="jsWave r-ways" >' + '<div class="way-wrapper col-way-1">' +
            '<img class="w_curl" src="/img/w_curl.png" alt="">' +
            '<img class="w_splash" src="/img/w_splash.png" alt="">' +
            '<div class="number_way">n%~</div>' + '<div class="number_member">m%</div>' + '</div>' +
            '</div>',
        w2: '<div data-id="%" class="jsWave r-ways" >' + '<div class="way-wrapper col-way-2">' +
            '<img class="w_curl" src="/img/w_curl.png" alt="">' +
            '<img class="w_splash" src="/img/w_splash.png" alt="">' +
            '<img class="w_curl w_curl-2" src="/img/w_curl.png" alt="">' +
            '<div class="number_way">n%~</div>' + '<div class="number_member">m%</div>' + '</div>' +
            '</div>',
        w3: '<div data-id="%" class="jsWave r-ways" >' + '<div class="way-wrapper col-way-3">' +
            '<img class="w_curl" src="/img/w_curl.png" alt="">' +
            '<img class="w_splash" src="/img/w_splash.png" alt="">' +
            '<img class="w_curl w_curl-2" src="/img/w_curl.png" alt="">' +
            '<img class="w_curl w_curl-3" src="/img/w_curl.png" alt="">' +
            '<div class="number_way">n%~</div>' + '<div class="number_member">m%</div>' + '</div>' +
            '</div>',
        w4: '<div data-id="%" class="jsWave r-ways" >' + '<div class="way-wrapper col-way-4">' +
            '<img class="w_curl" src="/img/w_curl.png" alt="">' +
            '<img class="w_splash" src="/img/w_splash.png" alt="">' +
            '<img class="w_curl w_curl-2" src="/img/w_curl.png" alt="">' +
            '<img class="w_curl w_curl-3" src="/img/w_curl.png" alt="">' +
            '<img class="w_curl w_curl-4" src="/img/w_curl.png" alt="">' +
            '<div class="number_way">n%~</div>' + '<div class="number_member">m%</div>' + '</div>' +
            '</div>',
        w5: '<div data-id="%" class="jsWave r-ways" >' + '<div class="way-wrapper col-way-5">' +
            '<img class="w_curl" src="/img/w_curl.png" alt="">' +
            '<img class="w_splash" src="/img/w_splash.png" alt="">' +
            '<img class="w_curl w_curl-2" src="/img/w_curl.png" alt="">' +
            '<img class="w_curl w_curl-3" src="/img/w_curl.png" alt="">' +
            '<img class="w_curl w_curl-4" src="/img/w_curl.png" alt="">' +
            '<img class="w_curl w_curl-5" src="/img/w_curl.png" alt="">' +
            '<div class="number_way">n%~</div>' + '<div class="number_member">m%</div>' + '</div>' +
            '</div>',
        w6: '<div data-id="%" class="jsWave r-ways" >' + '<div class="way-wrapper col-way-5">' +
            '<img class="w_curl" src="/img/w_curl.png" alt="">' +
            '<img class="w_splash" src="/img/w_splash.png" alt="">' +
            '<img class="w_curl w_curl-2" src="/img/w_curl.png" alt="">' +
            '<img class="w_curl w_curl-3" src="/img/w_curl.png" alt="">' +
            '<img class="w_curl w_curl-4" src="/img/w_curl.png" alt="">' +
            '<img class="w_curl w_curl-5" src="/img/w_curl.png" alt="">' +
            '<div class="number_way">n%~</div>' + '<div class="number_member">m%</div>' +
            '<img class="w_star"  src="/img/w_star.png" alt="">' + '</div>' + '</div>',
    },
    arrayModalBody: [
        'jsSignInModalBody',
        'jsRegisterModalBody',
        'jsChangeEmailModalBody',
        'jsAccessRecoveryModalBody',
        'jsAccessRecoveryModalBodyDone',
        'jsChangeEmailModalBody',
        'jsChangeEmailModalBodyDone',
        'jsRegisterModalBodyDone',
        'jsChangePasswordModalBody',
        'jsChangeLoginPhoneModalBody',
        'jsRecoveryPassModalBody',
        'jsAccessRecoveryPassModalBodyDone',
        'jsAccessRecoveryPassModalBody'
    ],
    arrayModal: [
        'jsSignInModal',
        'jsRegisterModal',
        'jsChangeEmailModalBody',
        'jsSignInDoneModal',
        'jsAccessRecoveryModal',
        'jsChangeLoginPhoneModal',
        'jsRecoveryPassModal',
        'jsAccessRecoveryPassModal'
    ]
};

(function () {
    var app = {
        // params: {
        //     arrayWaysTotals:[],
        //     waveLevelsArray: [
        //         [1, 2, 3, 4, 5, 6], [1, 6, 11, 16, 21, 25], [1, 26, 51, 76, 101, 125], [1, 126,
        // 257, 376, 501, 625], [1, 626, 1251, 1876, 2501, 3125], [1, 1326, 6251, 9376, 12501,
        // 15625], [1, 15626, 31251, 46876, 62501, 78125], [1, 78126, 156251, 234376, 312501,
        // 39625], [1, 390626, 781251, 1171876, 1562501, 1953125] ], arrayWaysType: { w1: '<div
        // data-id="%" class="jsWave r-ways" >' + '<div class="way-wrapper col-way-1">' + '<img
        // class="w_curl" src="/img/w_curl.png" alt="">' + '<img class="w_splash"
        // src="/img/w_splash.png" alt="">' + '<div class="number_way">n%~</div>' + '<div
        // class="number_member">m%</div>' + '</div>' + '</div>', w2: '<div data-id="%"
        // class="jsWave r-ways" >' + '<div class="way-wrapper col-way-2">' + '<img class="w_curl"
        // src="/img/w_curl.png" alt="">' + '<img class="w_splash" src="/img/w_splash.png" alt="">'
        // + '<img class="w_curl w_curl-2" src="/img/w_curl.png" alt="">' + '<div
        // class="number_way">n%~</div>' + '<div class="number_member">m%</div>' + '</div>' +
        // '</div>', w3: '<div data-id="%" class="jsWave r-ways" >' + '<div class="way-wrapper
        // col-way-3">' + '<img class="w_curl" src="/img/w_curl.png" alt="">' + '<img
        // class="w_splash" src="/img/w_splash.png" alt="">' + '<img class="w_curl w_curl-2"
        // src="/img/w_curl.png" alt="">' + '<img class="w_curl w_curl-3" src="/img/w_curl.png"
        // alt="">' + '<div class="number_way">n%~</div>' + '<div class="number_member">m%</div>' +
        // '</div>' + '</div>', w4: '<div data-id="%" class="jsWave r-ways" >' + '<div
        // class="way-wrapper col-way-4">' + '<img class="w_curl" src="/img/w_curl.png" alt="">' +
        // '<img class="w_splash" src="/img/w_splash.png" alt="">' + '<img class="w_curl w_curl-2"
        // src="/img/w_curl.png" alt="">' + '<img class="w_curl w_curl-3" src="/img/w_curl.png"
        // alt="">' + '<img class="w_curl w_curl-4" src="/img/w_curl.png" alt="">' + '<div
        // class="number_way">n%~</div>' + '<div class="number_member">m%</div>' + '</div>' +
        // '</div>', w5: '<div data-id="%" class="jsWave r-ways" >' + '<div class="way-wrapper
        // col-way-5">' + '<img class="w_curl" src="/img/w_curl.png" alt="">' + '<img
        // class="w_splash" src="/img/w_splash.png" alt="">' + '<img class="w_curl w_curl-2"
        // src="/img/w_curl.png" alt="">' + '<img class="w_curl w_curl-3" src="/img/w_curl.png"
        // alt="">' + '<img class="w_curl w_curl-4" src="/img/w_curl.png" alt="">' + '<img
        // class="w_curl w_curl-5" src="/img/w_curl.png" alt="">' + '<div
        // class="number_way">n%~</div>' + '<div class="number_member">m%</div>' + '</div>' +
        // '</div>', w6: '<div data-id="%" class="jsWave r-ways" >' + '<div class="way-wrapper
        // col-way-5">' + '<img class="w_curl" src="/img/w_curl.png" alt="">' + '<img
        // class="w_splash" src="/img/w_splash.png" alt="">' + '<img class="w_curl w_curl-2"
        // src="/img/w_curl.png" alt="">' + '<img class="w_curl w_curl-3" src="/img/w_curl.png"
        // alt="">' + '<img class="w_curl w_curl-4" src="/img/w_curl.png" alt="">' + '<img
        // class="w_curl w_curl-5" src="/img/w_curl.png" alt="">' + '<div
        // class="number_way">n%~</div>' + '<div class="number_member">m%</div>' + '<img
        // class="w_star"  src="/img/w_star.png" alt="">' + '</div>' + '</div>', }, arrayModalBody:
        // ['jsSignInModalBody', 'jsRegisterModalBody', 'jsAccessRecoveryModalBody',
        // 'jsAccessRecoveryModalBodyDone'], arrayModal: ['jsSignInModal', 'jsRegisterModal',
        // 'jsSignInDoneModal', 'jsAccessRecoveryModal'] },
        init: function () {
            this.main();
            this.event();
        },
        main: function () {

            $(".allCountry").intlTelInput({
                preferredCountries: [
                    "ru",
                    "ua",
                    "by",
                    "kz"
                ],
            });
            $('#loginPhone').val('+7');
            $('#InputLoginRegister').val('+7');

            // here is the functions run  when you load the page
            var moduleName = location.pathname.slice(1);
            var referralPath = location.search;

            var routeRegExp = {
                ways: new RegExp(/^ways.html$/),
                index: new RegExp(/^index.html$/),
                tasks: new RegExp(/^tasks.html$/),
                invitations: new RegExp(/^invitations.html$/),
                bonus: new RegExp(/^bonus.html$/),
                referral: new RegExp(/\?ref=(\+?\d+)/),
                user: new RegExp(/^user.html$/)
            };


            if ($.cookie('user_api_token') && $.cookie('user_api_token') !== undefined &&
                $.cookie('user_api_token') !== 'null') {
                if (routeRegExp.ways.test(moduleName)) {
                    view.setActiveUrl(1);
                    model.getUserModelById(view.drawAllWaves);
                } else if (routeRegExp.index.test(moduleName) || moduleName === '') {
                    model.getUserModelById(function () {
                    });
                } else if (routeRegExp.tasks.test(moduleName)) {
                    view.setActiveUrl(2);
                    model.getUserModelById(function () {
                        // if(window.app.user.current_task == undefined ||
                        // window.app.user.current_task == null ){ window.app.user.current_task =
                        // 0; }
                        model.getTaskList(function () {

                            window.app.user.taskList.forEach(function (item, index, arr) {
                                if (item._id == window.app.user.current_task) {
                                    window.app.user.current_task_n = index;
                                }
                            });
                            view.drawTask(window.app.user.current_task_n);
                        });
                    });
                } else if (routeRegExp.invitations.test(moduleName)) {
                    view.setActiveUrl(0);
                    model.getUserModelById(view.creatReferralBtn);
                } else if (routeRegExp.bonus.test(moduleName)) {
                    view.setActiveUrl(3);
                    model.getUserModelById(function () {
                    });
                } else if (routeRegExp.user.test(moduleName)) {
                    model.getUserModelById(view.drawUserPage);
                }
            } else {
                if (routeRegExp.index.test(moduleName) || moduleName === '') {
                    window.app.referral = '';
                    if (referralPath) {
                        window.app.referral = referralPath.match(routeRegExp.referral)[1];
                        view.showRegisterPopup();
                    } else {
                        view.showSignInPopup();
                    }
                } else {
                    window.location.href = '/';
                }
            }
        },
        event: function () { // here pinning listeners to the events
            $(document).ready(function () {
                $('.jsBtnLogin').on('click', view.showSignInPopup);
                // $('.jsBtnLoginAction').on('click', controller.pressClickBtnLoginAction);
                $('.jsBtnLoginAction').on('click', controller.pressClickBtnLoginPassAction);
                $('.jsBtnRegister').on('click', view.showRegisterPopup);
                $('.jsBtnGetSMSRegister').on('click', controller.pressClickBtnRegisterGetSMS);
                $('.jsBtnRegisterSmsOk').on('click', controller.pressClickBtnRegisterSMSOk);
                $('.jsBtnGetSMSRecoveryPass').on('click',
                        controller.pressClickBtnRecoveryPassGetSMS);
                $('.jsBtnRecoveryPassSmsOk').on('click', controller.pressClickBtnReloadPassSMSOk);
                $('.jsBtnRecoveryPassAction').on('click',
                        controller.pressClickBtnRecoveryPassAction);

                $('.jsBtnGetSMS').on('click', controller.pressClickBtnGetSMS);
                $('.jsBtnRegisterAction').on('click', controller.pressClickBtnRegisterAction);
                $('.btnAction_done').on('click', controller.pressClickBtnAction_done);
                $('.jsBtnExit').on('click', controller.pressClickBtnExit);
                $('.jsBtnAccessRecovery').on('click', view.showAccessRecovery);
                $('.jsBtnAccessRecoveryAction').on('click',
                        controller.pressClickBtnRecoveryPassEmail);

                // $('.jsChangeLoginPhoneModalBody .jsInputSMS').on('click',
                // controller.pressClickBtnChangeLoginPhoneActive);

                $('.block-ways-row').on('click', '.jsWave', controller.singleWave);

                $('#modalPopupSinglWave').on('hide.bs.modal', view.clearSingleWaveInfo);
                $('.jsReferralLink').on('click', view.showTooltipCopyUrlReferral);
                $('.jsSubMenu').on('focus', view.showSubMenu);

                $('.jsSubmenu').hover(function () {
                    $('.sub-menu').show();
                });
                // $('.jsSubmenu').click(function(e){ e.preventDefault() });
                $('.li-menu').hover(function () {
                    $('.sub-menu').hide();
                });
                // $('.JsPersonalArea').on('click', view.showPersonalAreaPopup);
                $('.jsBtnAccessSaveUserAreaAction').on('click', function (e) {
                    e.preventDefault()
                });
                $('.jsTaskBtnDone').on('click', controller.pressClickTaskDone);
                $('.jsBtnPrev').on('click', function (e) {
                    var idTask = $('#title-task').attr('data-task');
                    view.drawTask(parseInt(idTask) - 1);
                });
                // $('.jsSaveUserSetting').on('click', controller.pressClickSaveUserSetting);
                $('.jsSaveUserSetting').on('click', controller.pressClickSaveUserSetting);
                $('.jsChangeEmail').on('click', controller.pressClickChangeEmail);
                $('.jsChangePassword').on('click', controller.pressClickChangePassword);
                $('li.country').on('click', controller.selectCountry);
                $('.jsBtnAccessChangeEmailAction').on('click',
                        controller.pressClickBtnChangeEmailActive);
                $('.jsBtnAccessChangePasAction').on('click',
                        controller.pressClickBtnChangePasActive);
                $('.jsChangeLoginPhone').on('click', controller.pressClickBtnChangeLoginPhone);
                $('.jsBtnGeLogintSMS').on('click', controller.pressClickBtnChangeLoginPhoneGetSMS);
                $('.jsBtnAccessChangeLogisPhoneAction').on('click',
                        controller.pressClickBtnChangeLoginPhoneActive);

                $('#jsOther').click(function (e) {
                    e.preventDefault()
                });
                $('.jsConfirmEmail').click(function (e) {
                    e.preventDefault()
                });

                $('.jsSelectCategory').on('change', controller.changeSelectCategory);
                $('.jsSetupEmail').on('click', controller.setupEmail);
                $('.jsBtnSetupEmailOk').on('click', controller.setupEmailOfInput);
                $('.jsForgotPass').on('click', function (e) {
                    e.preventDefault();
                    $('.jsForgotPassBlock').removeClass('hidden');
                });

                $('.jsOfferLink').on('click', function () {
                });

            });
        }
    };
    app.init();
    // return app.params;
}());

